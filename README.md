Script(s) that grew way past what I originally intended, to show information about various events in the game. Currently gathers data about the following:

* Spawned enemies and loot (separated by type or aggregated)
* Spawned SWAT turrets
* Spawned civilians, hostages (civilians and enemies)
* Jokered enemies (with owner, health, upgrades and a kill counter)
* Some special pickups, e.g. keycards and gage packets
* Drill/hack timers
* Deployed equipment (owner, status)
* Active ECMs
* Active pagers
* Active tape loops
* Various buffs and player effects

All of these can be disabled as you please by changing a flag in the HUDListManager.ListOptions, which also lets you change the scale of the items.

The script is split into two separate files; the GameInfoManager (lacking a better name) gathers and processes data behind the scenes and provides the core logic. The HUDList file provides the functionality for showing it on the HUD as well as a bit of logic for grouping/organizing the information.

The HUDList relies on the GameInfoManager and won't function without it, but the latter can be used separately. If using it as stand-alone, it will do nothing unless you register listeners for use in your own scripts. For instance, if you wish to have a script react to a drill starting/stopping, you can register an event with the following line:

	managers.gameinfo:register_listener(<listener_id>, "timer", "set_active", <callback>)
	
where you define a unique ID for your listener to reference if you want to remove it later, and a callback function to do something when the notification is received. The various events you can listen for and what the callback function parameters are is poorly documented, but the HUDList code provides plenty of examples.

(Pardon the commit history for the post-patch-100 version. I should've branched but the beta caught me in the middle of a refactoring period and I just went with it and got to pay for it.)

To get back to the main portal repository, [click me!](https://bitbucket.org/pjal3urb/pd2-mods)
